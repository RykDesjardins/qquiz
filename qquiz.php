<?php
/*
Plugin Name:	Qquiz
Plugin URI:	
Description:	Qquiz offers a quick and simple way to create personality tests for your Wordpress site.
Version:	0.1 Beta
Author:		Érik Desjardins
Author URI:	erikdesjardins.com
License:	GPLv2
*/	
?><?php

add_action('init', 'qquiz_create_quiz_post_type');
add_action('admin_init', 'qquiz_admin_init');
add_action('wpmu_new_blog', 'qquiz_new_network_site');
add_action('admin_enqueue_scripts', 'qquiz_load_css');
add_action('admin_enqueue_scripts', 'qquiz_load_js');
add_action('save_post_qquiz', 'qquiz_save_custom_fields');
add_action('wp_head', 'qquiz_add_meta');

add_action('wp_ajax_qquiz_answer_quiz', 'qquiz_user_answered_quiz');
add_action('wp_ajax_nopriv_qquiz_answer_quiz', 'qquiz_user_answered_quiz');

add_filter('pre_get_posts', 'add_quiz_to_loop');

add_shortcode('the-quiz', 'print_the_quiz');

register_activation_hook(__FILE__, 'qquiz_activation');

function qquiz_create_quiz_post_type() {
	register_post_type('qquiz',
		array(
			'labels' => array(
				'name' => 'Quizzes',
				'singular_name' => 'Quiz',
				'add_new' => 'Create New',
				'add_new_item' => 'Create New Quiz',
				'edit' => 'Edit',
				'edit_item' => 'Edit Quiz',
				'new_item' => 'New Quiz',
				'view' => 'View',
				'view_item' => 'View Quiz',
				'search_items' => 'Search Quiz',
				'not_found' => 'No Quiz found',
				'not_found_in_trash' => 'No Quiz found in Trash',
				'parent' => 'Parent Quiz'
			),
			'public' => true,
			'menu_position' => 20,
		    'publicly_queryable' => TRUE,
			'rewrite' => array('slug' => 'quizzes','with_front' => false),
			'supports' => array('title', 'editor', 'categories', 'thumbnail', 'custom-fields', 'author', 'advanced-custom-fields'),
            'taxonomies' => array('post_tag'),
			'menu_icon' => 'dashicons-clipboard',
			'has_archive' => true
		)
	);
	
	flush_rewrite_rules();
}

function qquiz_add_meta() {
	global $post; if (get_post_type( $post ) == "qquiz") {
		echo '<meta property="og:type" content="article" /> ';
	}
	
	if (isset($_GET['l'])) {
		qquiz_get_from_label(get_the_ID(), $_GET['l']);
	}
}

function qquiz_load_css() {
	global $typenow; if ($typenow=="qquiz") {
		echo  "<link type='text/css' rel='stylesheet' href='" . plugins_url('qquiz_admin_styles.css', __FILE__) . "' />";
	}	
}

function qquiz_load_js() {
	global $typenow; if ($typenow=="qquiz") {
		wp_enqueue_media();
		wp_register_script('qquiz_admin_js', plugins_url('qquiz_admin_js.js', __FILE__), array('jquery'));
		wp_enqueue_script('qquiz_admin_js');

		echo '<script>var qQuizURL = "'.plugins_url('', __FILE__).'"</script>';
	}	
}

function qquiz_get_from_label($id, $lbl) {
	global $wpdb;
	$q = "SELECT * FROM ".$wpdb->get_blog_prefix()."qquiz_post_pers WHERE qquiz_post_id = ".$id." AND qquiz_pers_label = '" . $lbl . "'";
	$result = $wpdb->get_results($q);
	
	foreach ( $result as $pers ) {
		echo '<meta property="og:title" content="' . str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($pers->qquiz_pers_name)) . '" />';
		echo '<meta property="og:description" content="' . str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($pers->qquiz_pers_desc)) . '" />';
		echo '<meta property="og:image" content="' . $pers->qquiz_pers_attach . '" />';
		echo '<meta property="og:type" content="mtlblog:quiz" /> ';
	}
}

function qquiz_admin_init() {
	add_meta_box('qquiz_personalities_meta_box',
		'Personalities and labels',
		'qquiz_display_personalities_meta_box',
		'qquiz', 'normal', 'high');	

	add_meta_box('qquiz_quests_meta_box',
		'Questions and answers',
		'qquiz_display_quests_meta_box',
		'qquiz', 'normal', 'high');
}

function qquiz_display_personalities_meta_box($qquiz) {
	include_once('qquiz_meta_pers.php');
}

function qquiz_display_quests_meta_box($qquiz) {
	include_once('qquiz_meta_quests.php');
}

function qquiz_activation() {
	global $wpdb;

	if ( is_multisite() ) {
		if ( !empty( $_GET['networkwide'] ) ) {
			$start_blog = $wpdb->blogid;
			$blog_list = $wpdb->get_col( 'SELECT blog_id FROM ' . $wpdb->blogs );

			foreach ( $blog_list as $blog ) {
				switch_to_blog( $blog );
				qquiz_create_table( $wpdb->get_blog_prefix() );
			}

			switch_to_blog( $start_blog );
			return;
		}
	}

	qquiz_create_table($wpdb->get_blog_prefix());
}

function qquiz_create_table($prefix) {
	global $wpdb;

	$creation_query = 
		'CREATE TABLE IF NOT EXISTS ' .$prefix. 'qquiz_post_pers (
			`qquiz_post_id` int(20) NOT NULL,
			`qquiz_pers_name` varchar(100),
			`qquiz_pers_desc` text,
			`qquiz_pers_attach` text,
			`qquiz_pers_label` varchar(10),
			`qquiz_pers_cred_name` varchar(100),
			`qquiz_pers_cred_link` varchar(500)		
		); ';

	$wpdb->query($creation_query);

	$creation_query = 
		'CREATE TABLE IF NOT EXISTS ' .$prefix. 'qquiz_post_quest (
			`qquiz_post_quest_id` int(20) NOT NULL AUTO_INCREMENT,
			`qquiz_post_id` int(20) NOT NULL,
			`qquiz_quest_index` int(6) NOT NULL,
			`qquiz_quest_text` text,
			`qquiz_quest_attach` text,
			`qquiz_quest_cred_name` varchar(100),
			`qquiz_quest_cred_link` varchar(500),	
			PRIMARY KEY (`qquiz_post_quest_id`)		
		);';

	$wpdb->query($creation_query);

	$creation_query = 
		'CREATE TABLE IF NOT EXISTS ' .$prefix. 'qquiz_quest_answer (
			`qquiz_post_id` int(20) NOT NULL,
			`qquiz_quest_index` int(6) NOT NULL,
			`qquiz_answer_text` text,
			`qquiz_answer_attach` text,
			`qquiz_answer_type` varchar(5),
			`qquiz_answer_cred_name` varchar(100),
			`qquiz_answer_cred_link` varchar(500),		
			`qquiz_pers_label` varchar(50)			
		);';

	$wpdb->query($creation_query);
	
	$creation_query = 
		'CREATE TABLE IF NOT EXISTS ' .$prefix. 'qquiz_answered (
			`qquiz_post_id` int(20) NOT NULL,
            `qquiz_blog_id` int(20) NOT NULL,
			`qquiz_user_id` int(6) NOT NULL,
			`qquiz_pers_name` text,
			`qquiz_date` date		
		);';

	$wpdb->query($creation_query);	
}

function qquiz_new_network_site($blogid) {
	global $wpdb;

	if (!function_exists( 'is_plugin_active_for_network' ) ) {
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	}

	if (is_plugin_active_for_network(plugin_basename(__FILE__))) {
		$start_blog = $wpdb->blogid;
		switch_to_blog( $blog_id );
		
		qquiz_create_table( $wpdb->get_blog_prefix() );
		switch_to_blog( $start_blog );
	}	
}

function qquiz_save_custom_fields($post_id) {
	global $wpdb;
	$prefix = $wpdb->get_blog_prefix();

	if (isset($_POST['qquiz_pers_name'])) {
		$wpdb->query("DELETE FROM " .$prefix. "qquiz_post_pers WHERE `qquiz_post_id` = " . $post_id);
		$wpdb->query("DELETE FROM " .$prefix. "qquiz_post_quest WHERE `qquiz_post_id` = " . $post_id);
		$wpdb->query("DELETE FROM " .$prefix. "qquiz_quest_answer WHERE `qquiz_post_id` = " . $post_id);
		
		// Loop through personalities
		for ($i = 0; $i < count($_POST["qquiz_pers_label"]); $i = $i + 1) {
			if (trim($_POST["qquiz_pers_label"][$i]) != '') {
				$wpdb->query("INSERT INTO " .$prefix. "qquiz_post_pers VALUES(".
					$post_id.", '".
					$_POST["qquiz_pers_name"][$i]."','".
					$_POST["qquiz_pers_desc"][$i]."','".
					$_POST["newPersImgScr"][$i]."','".
					$_POST["qquiz_pers_label"][$i]."','".
					$_POST["qquiz_pers_cred_name"][$i]."','".
					$_POST["qquiz_pers_cred_link"][$i]."')"
				);
			}
		}
		
		// Loop through questions
		for ($i = 0; $i < count($_POST["question_index"]); $i = $i + 1) {
			if (trim($_POST["question_name"][$i]) != '') {
				$wpdb->query("INSERT INTO " .$prefix. "qquiz_post_quest VALUES(NULL, ".
					$post_id.", ".
					$_POST["question_index"][$i].", '".
					$_POST["question_name"][$i]."','".
					$_POST["question_image"][$i]."','".
					$_POST["question_cred_name"][$i]."','".
					$_POST["question_cred_link"][$i]."')"
				);
			}
		}
		
		// Loop through answers
		for ($i = 0; $i < count($_POST["quest_answer_index"]); $i = $i + 1) {
			if (trim($_POST["answer_label"][$i]) != '') {
				$wpdb->query("INSERT INTO " .$prefix. "qquiz_quest_answer VALUES(".
					$post_id.", ".
					$_POST["quest_answer_index"][$i].", '".         
					$_POST["answer_name"][$i]."', '".
					$_POST["answer_image"][$i]."', 'ans','".
					$_POST["answer_label"][$i]."','".
					$_POST["answer_cred_name"][$i]."','".
					$_POST["answer_cred_link"][$i]."')"
				);        
			}
		}
	}    
}	

function add_quiz_to_loop( $query ) {
	if (is_home() && $query->is_main_query()) {
		$query->set('post_type', array('post', 'qquiz'));
	} 
	
	return $query;
}

function qquiz_user_answered_quiz() {
	global $wpdb;
	
	$userid = get_current_user_ID();
	$postid = $_POST['postid'];
	$pers = $_POST['pers'];
	$prefix = $wpdb->base_prefix;
    $blogid = get_current_blog_id();
	
	$q = "DELETE FROM ".$prefix."qquiz_answered WHERE qquiz_post_id=".$postid." AND qquiz_blog_id = $blogid AND qquiz_user_id=".$userid;
	$wpdb->query($q);
	
	$q = "INSERT INTO ".$prefix."qquiz_answered VALUES(".$postid.", $blogid, ".$userid.", '".$pers."', CURDATE())";
	$wpdb->query($q);
	
	echo 'OK ; ';
}

function print_the_quiz() {
	ob_start();
	require 'qquiz_presentation.php';
	return ob_get_clean();
}

?>
