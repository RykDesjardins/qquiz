<?php
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);

	define('DOING_AJAX', true);
	define( 'SHORTINIT', true );
	$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	require_once( $DOCUMENT_ROOT . '/wp-load.php' );
	require_once( $DOCUMENT_ROOT . '/wp-settings.php' );
	require_once( $DOCUMENT_ROOT . '/wp-includes/session.php' );
	require_once( $DOCUMENT_ROOT . '/wp-includes/capabilities.php' );
	require_once( $DOCUMENT_ROOT . '/wp-includes/pluggable.php' );
	require_once( $DOCUMENT_ROOT . '/wp-includes/user.php' );
	require_once( $DOCUMENT_ROOT . '/wp-includes/meta.php' );
	require_once( $DOCUMENT_ROOT . '/wp-includes/query.php' );
	
	session_start();
	global $current_user;
	get_currentuserinfo();

	function qquiz_user_answered($qquiz, $user, $answer) {
		global $wpdb;
		$prefix = $wpdb->get_blog_prefix();

		$q = 
			"INSERT INTO " .$prefix. "qquiz_answered VALUES(
				".$qquiz.",
				".$user.",
				'".$answer."',
				CURDATE()
			);";
			
		$wpdb->query($q);
	}
	
	$act_type = $_POST['t'];
	
	switch ($act_type) {
	case 'qquiz': 
		$qquiz_id = $_POST['q'];
		$answer = $_POST['a'];
		
		qquiz_user_answered($qquiz_id, $current_user->ID, $answer);
		echo uniqid();
		
		break;
	}
?>