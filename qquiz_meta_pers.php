<?php 
	// Default meta box for personalities definitions
	global $wpdb;
	$personalities = array();
	$columncount = 5;

	if (!empty($_GET['post'])) {
		$q = '
			SELECT * from '.$wpdb->get_blog_prefix().'qquiz_post_pers
			WHERE qquiz_post_id = '.$_GET['post'].'
		;';

		$result = $wpdb->get_results($q);
		
		foreach ( $result as $pers ) {
			array_push($personalities, $pers);
		}
	} 
?>

	<script>
		// Filled on update, blank on create
		var personalities = [
			<?php
				for ($i = 0; $i < count($personalities); $i++) {
					echo ($i == 0 ? "" : ",") . 
						'{"name":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_name)).
						 '","desc":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_desc)).
						 '","attach":"'.$personalities[$i]->qquiz_pers_attach.
						 '","label":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_label)).
						 '","credname":"'.$personalities[$i]->qquiz_pers_cred_name.
						 '","credlink":"'.$personalities[$i]->qquiz_pers_cred_link.
						 '"}';
				}				
			?>
		];
		
		// Once DOM is ready
		jQuery(function() {
			// HTML generator and event manager
			var generator = new QquizHTMLGenerator();
			var eventManager = new QquizEventManager();

			// For each personalities
			for (var i = 0; i < personalities.length; i++) {
				jQuery('.qquiz-pers-table tbody').append(
					generator.generateNewPersonality(personalities[i])
				);
			}

			// Bind attachment click
			eventManager.bindAllPersThumbnailClicks();
		});
	</script>


	<div>
		<table class="qquiz-pers-table">
			<thead>
				<th>Personality name</th>
				<th>Description</th>
				<th>Attachment</th>
				<th>Short label</th>
				<th></th>
			</thead>
			<tbody>
				<!-- Loop for all personalities -->
				<!-- End loop -->
			</tbody>
		</table>
	
		<footer>
			<button id="qquiz-btn-addpers" class="button">Add personality</button>
		</footer>
	</div>	
<?php ?>


