<?php
	// User presentation of a Qquiz object
	global $post;
	global $wpdb;
	
	$questions = array();
	$answers = array();	
	$personalities = array();

	$q = '
		SELECT * from '.$wpdb->get_blog_prefix().'qquiz_post_pers
		WHERE qquiz_post_id = '.get_the_ID().'
	;';

	$result = $wpdb->get_results($q);
	
	foreach ( $result as $pers ) {
		array_push($personalities, $pers);
	}
	
	// Database requests
	$q = '
		SELECT * from '.$wpdb->get_blog_prefix().'qquiz_post_quest
		WHERE qquiz_post_id = '.get_the_ID().'
	;';

	$result = $wpdb->get_results($q);
	
	foreach ( $result as $pers ) {
		array_push($questions, $pers);
	}

	$q = '
		SELECT * from '.$wpdb->get_blog_prefix().'qquiz_quest_answer
		WHERE qquiz_post_id = '.get_the_ID().'
	;';

	$result = $wpdb->get_results($q);
	
	foreach ( $result as $res ) {
		array_push($answers, $res);
	}
	
	$aloop = 0;
	$qloop = 0;
?>

<style>
	<?php if ($_GET['preview'] == 'true') : ?>
	#qquiz-debug-float {
		display: block;
		position: fixed;
		top:40px;
		right: 10px;

		background-color: rgba( 255, 255, 255, 0.95);
		z-index: 10000;
	
		margin:0;
		padding:0;

		border: 3px solid rgba( 180, 180, 180, 0.9);

		font-size: 12px;
	}

	#qquiz-debug-float td, #qquiz-debug-float th {
		padding: 3px;
	}

	#qquiz-debug-float .title {
		font-weight: bold;
		text-align: center;
	}	
	<?php endif; ?>

  .qquiz-question-wrapper {
    margin-bottom:16px;
  }

  .qquiz-the-container h3 {
	display: block;
	padding-top: 10px;
  }

  .qquiz-question-image {
    width:100%;
    
    margin-bottom:0px;
  }
  
  .qquiz-answers-wrapper .qquiz-answer-single {
    display: inline-block;
    vertical-align:top;
    margin:10px;
    padding:10px;
    
    border: 1px solid #DDD;
    background-color: #EEE;
    
    cursor: pointer;
    font-weight: bold;
  }
  
  .qquiz-answers-wrapper.noimage .qquiz-answer-single {
    display:block;
  }
  
  .qquiz-answers-wrapper .qquiz-answer-image {
    height:195px;
    width:195px;
    background-size: cover;

	position: relative;
  }

  .qquiz-answers-wrapper .qquiz-answer-image .qquiz-answer-cred-wrapper {
	position: absolute;

	bottom:0px;
	right:0px;
	padding: 3px 6px;

	background-color: rgba(0, 0, 0, 0.2);
	opacity: 0.5;
  }

  .qquiz-answers-wrapper .qquiz-answer-image .qquiz-answer-cred-wrapper:hover {
	opacity: 0.8;
	background-color: rgba(0, 0, 0, 0.5);
  }

  .qquiz-answers-wrapper .qquiz-answer-image .qquiz-answer-cred-wrapper a {
	color: #FFF;
	font-size: 10px;
  }

  .qquiz-answers-wrapper .qquiz-answer-image .qquiz-answer-cred-wrapper a:hover {
	text-decoration: underline;
  }
  
  .qquiz-answers-wrapper.noimage .qquiz-answer-image {
    display:none;
  }
  
  .qquiz-answer-single.leftover * {
    opacity:0.5;
  }
  
  .qquiz-answer-single.leftover:hover {
    background-color: #EEE;
    color: #000;
  }
  
  .qquiz-answer-single:hover,
  .qquiz-answer-single.selected {
    background-color: #FF4200;
    color: #FFF;
  }
  
  .qquiz-answer-single span {
    display:block;
    width:195px;
    
    margin-top:4px;
    font-size: 12px;
  }
  
  .qquiz-answers-wrapper.noimage .qquiz-answer-single span {
    display:block;
    width:auto;
    margin:0;
    font-size: 16px;	
  }
  
  .qquiz-personality-wrapper {
    display:block;
    
    border: 1px solid #DDD;
    padding:15px;
    
    background-color: #EEE;
  }
  
  .qquiz-personality-wrapper h2 {
    font-family: Oswald,sans-serif;
    color:#777;
  }
  
  .qquiz-personality-wrapper h3 {
    
  }
  
  .qquiz-personality-halfblock {
    display:inline-block;
    width: 49%; 
    vertical-align: top;
  }
  
  .qquiz-personality-desc {
    vertical-align: top;
    color: #777;
    
    font-style: italic;
    padding-left:16px;
  }
  
  .qquiz-hidden {
    display:none;
  }
  
  .qquiz-personality-social {
    margin: 15px -15px -15px;
    padding: 15px;
    border-top: 1px solid #DDD;
    background-color: #F5F5F5;
  }
  
  .qquiz-personality-social h4 {
    font-family: Oswald,sans-serif;
    
  }

  .qquiz-personality-share {
    padding:4px;
    display:inline-block;
    
    margin-right:8px;
    
    -wekbit-border-radius: 3px;
       -moz-border-radius: 3px;
	-ms-border-radius: 3px;
	 -o-border-radius: 3px;
	    border-radius: 3px;
    
    cursor: pointer;
  }  
  
  .qquiz-personality-share img {
    width:32px;
    height:32px;
  }
  
  .qquiz-personality-share span {
    color: rgb(255, 255, 255);
    font-weight: bold;
    vertical-align: baseline;
    margin: 10px;  
  }
  
  .qquiz-personality-fbshare {
    background-color:#3B5998;
  }
  
  .qquiz-personality-twshare {
    background-color:#62BBFF;
  }  

  .qquiz-question-cred-wrapper a {
	color: #999;
	font-size: 10px;
	margin-top:0px;
  }

  .qquiz-question-cred-wrapper a:hover {
	text-decoration: underline;
  }

  @media only screen and (max-width: 768px) {
	.qquiz-debug-float {
	  display: none;
	}

	.qquiz-answers-wrapper .qquiz-answer-single {
	  margin: 6px;
	  padding: 6px;
	}

	.qquiz-personality-desc {
	  padding: 0px;
	}
	
    .qquiz-personality-halfblock {
      display:block;
      width: auto;
    }
	
    .qquiz-personality-halfblock .qquiz-personality-image {
      width:auto;
      margin-bottom: 20px;
    }

	.qquiz-answers-wrapper .qquiz-answer-image {
	  width: 110px;
	  height: 110px;
	}

	.qquiz-personality-share {
	  margin-right: 0px;
	}

	.qquiz-personality-share span {
	  margin: 5px;
	}

    .qquiz-personality-halfblock .qquiz-personality-share {
      margin-bottom: 12px;
    }

    .qquiz-answers-wrapper {
      text-align: center;
    }
   
    .qquiz-answer-single span {
      font-size: 22px;
	  width: auto;
    }
    
    .qquiz-answers-wrapper.noimage .qquiz-answer-single span {
      font-size: 18px;
    }

	.qquiz-answers-wrapper .qquiz-answer-image .qquiz-answer-cred-wrapper {
	  padding: 0px 2px;
	}

	.qquiz-answers-wrapper .qquiz-answer-image .qquiz-answer-cred-wrapper a {
	  font-size: 9px;

	  pointer-events: none;
	  cursor: default;
	}
  }
</style>

<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '<?=get_option("facebook_app_id")?>',
			cookie     : true, 
			xfbml      : true, 
			version    : 'v2.1' 
		});

		that.getState(function(response) {
			that._statusCB[response.status]();
			that.currentStatus = response.status;
		});
	}

  var _qquiz_g = {
    quest_count: <?=count($questions)?>,
    quest_answered: 0,
    pers: {
	<?php for ($i = 0; $i < count($personalities); $i++) {
		echo ($i == 0 ? "" : ",") . 
			'"'.preg_replace('/\s+/', '', str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_label))).
			  '":{"name":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_name)).
			  '","desc":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_desc)).
			  '","attach":"'.$personalities[$i]->qquiz_pers_attach.
			  '","count":0}';
	}?>	
    }
  };

  var checkIfFinished = function() {
    if (_qquiz_g.quest_count == _qquiz_g.quest_answered) {
      // Lock answers
      $('.qquiz-answer-single').unbind('click');
      castPers();
    }
  };
  
  var castPers = function() {
    var theBest = {count:0};
    
    for (var key in _qquiz_g.pers) {
      if (_qquiz_g.pers[key].count >= theBest.count) {
	theBest = _qquiz_g.pers[key];
      }
    }
  
    $('.qquiz-personality-message').hide();
    $('.qquiz-personality-answer').html(theBest.name);
    $('.qquiz-personality-desc').html(theBest.desc);
    $('.qquiz-personality-image').attr('src', theBest.attach);
    
    $('.qquiz-personality').removeClass('qquiz-hidden');
    
    $('html, body').animate({
        scrollTop: $("#qquiz-personality").offset().top - 100
    }, 600);    
    
    // Trigger read on quiz
    $.ajax({
      url:"/wp-admin/admin-ajax.php",
      type:'POST',
      data: {"action":"qquiz_answer_quiz","postid":<?=get_the_ID()?>,"udi":"mtlblog-<?=uniqid()?>","pers":theBest.name},
      success : function(rep) {
		// alert(rep);
      }
    });
  };

  var castFacebookShareDialog = function() {
    var theBest = {count:0};
    
    for (var key in _qquiz_g.pers) {
      if (_qquiz_g.pers[key].count >= theBest.count) {
		theBest = _qquiz_g.pers[key];
      }
    }

	FB.ui({ method: 'feed', 
		   caption: '<?=get_site_url()?>',
		   description: theBest.desc,
		   display: 'popup',
		   link: '<?=the_permalink()?>',
		   name: "<?=v("quiz_i_got")?> " + theBest.name + "! <?=the_title()?>",
		   picture: theBest.attach
	});
  }

  var castTwitterShareDialog = function(caller) {
    var theBest = {count:0};
    
    for (var key in _qquiz_g.pers) {
      if (_qquiz_g.pers[key].count >= theBest.count) {
		theBest = _qquiz_g.pers[key];
      }
    }

	var title = jQuery("<textarea />").html("<?=wp_specialchars_decode(wp_kses_decode_entities(the_title()))?>").text();
	

	var url = "https://twitter.com/intent/tweet?text=<?=v("quiz_i_got")?> "+theBest.name+"! "+title+"&url=<?=the_permalink()?>&via=<?=get_option('twitter_name')?>";

	window.open(url, '_blank', 'toolbar=0,location=0,menubar=0,width=575,height=400');
  };

  var loadQuiz = function() {
    var $ = jQuery;
	var DEBUG = <?= $_GET['preview'] == 'true' ? 'true' : 'false'; ?>;
    
	/*
    $('.qquiz-answer-image:not(.noimage)').each(function(index, img) {
      var path = 'url(' + $(img).data('image') + ')';
      $(img).css('background-image', path);
    });
	*/
    
    $('.qquiz-answer-single').bind('click', function() {
      var selected = $(this).parent().find('.qquiz-answer-single.selected');
      
	  var codes = $(this).data('pcode').replace(/ /g,'').split(',');

	  if (!selected.length) {
		  _qquiz_g.quest_answered +=1;
	  }

	  if (!$(this).hasClass('selected')) {
			if (selected.length == 1) {
			  selected.removeClass('selected');
			  var oldcodes = selected.data('pcode').replace(/ /g,'').split(',');

			  for (var oldindex = 0; oldindex < oldcodes.length; oldindex++) {
			  	_qquiz_g.pers[oldcodes[oldindex]].count -= 1;
			  }
			} 

		  for (var index = 0; index < codes.length; index++) {
				$(this).parent().find('.qquiz-answer-single').addClass('leftover');
				$(this).removeClass('leftover').addClass('selected');

				_qquiz_g.pers[codes[index]].count += 1;
		  }
      }

	  if (DEBUG) {
		for (var key in _qquiz_g.pers) {
		  $('#qquiz-debug-float .DEBUG_' + key).html(_qquiz_g.pers[key].count);
		}
	  }

      checkIfFinished();
    });

	if (DEBUG) {
	  $('body').prepend($('#qquiz-debug-float'));
	}
    
  };

  document.addEventListener("DOMContentLoaded", loadQuiz, false);
</script>

<?php if ($_GET['preview'] == 'true') : ?>
	<div id="qquiz-debug-float">
		<table>
			<thead>
				<tr>
					<th colspan="2" class="title">Quiz debugging</th>
				</tr>
				<tr>
					<th>Answer</th>
					<th>Score</th>
				</tr>
			</thead>
			<tbody>
				<?php for ($i = 0; $i < count($personalities); $i++) {
					echo '<tr><td>';
					echo str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_name));
					echo '</td><td class="DEBUG_'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($personalities[$i]->qquiz_pers_label)).'">0</td></tr>';
				}?>					
			</tbody>
		</table>
	</div>
<?php endif; ?>

<div class="qquiz-the-container">
  <?php foreach ($questions as $q) : $qloop += 1; ?>
	<hr />
    <div class="qquiz-question-wrapper">
      <h3><?=$q->qquiz_quest_text?></h3>
      <?php if ($q->qquiz_quest_attach != '') : ?>
      <div class="qquiz-question-image-div">
		<img class="qquiz-question-image" src="<?=$q->qquiz_quest_attach?>" />
      </div>
	  <div class="qquiz-question-cred-wrapper">
		<a class="qquiz-question-photo-cred" href="<?=$q->qquiz_quest_cred_link?>"><?=$q->qquiz_quest_cred_name?></a>
	  </div>
      <?php endif ?>
      
      <div class="qquiz-answers-wrapper <?=$answers[$aloop]->qquiz_answer_attach==''?' noimage':''?>">
		<?php for ($qloop; $answers[$aloop]->qquiz_quest_index==$q->qquiz_quest_index;$aloop+=1) : ?>
		  <div class="qquiz-answer-single" data-pcode="<?=preg_replace('/\s+/', '', $answers[$aloop]->qquiz_pers_label)?>">
			<div class="qquiz-answer-image<?=$answers[$aloop]->qquiz_answer_attach==''?' noimage':''?>" 
				 data-image="<?=$answers[$aloop]->qquiz_answer_attach?>" style="background-image:url(<?=$answers[$aloop]->qquiz_answer_attach?>)">
				<div class="qquiz-answer-cred-wrapper">
					<a class="qquiz-question-photo-cred" href="<?=$answers[$aloop]->qquiz_answer_cred_link?>"><?=$answers[$aloop]->qquiz_answer_cred_name?></a>
				</div>
			</div>
			<span><?=$answers[$aloop]->qquiz_answer_text?></span>
		  </div>
		<?php endfor ?>
      </div>
    </div>
  <?php endforeach ?>

  <hr />
  <div class="qquiz-personality-wrapper" id="qquiz-personality">
    <span class="qquiz-personality-message"><?=v("quiz_not_finished")?></span>
    <div class="qquiz-personality qquiz-hidden">
      <h2 class="qquiz-personality-quest"><?=the_title()?></h2>
      <h3 class="qquiz-personality-answer"></h3>
      <div class="qquiz-personality-details">
	<div class="qquiz-personality-halfblock">
	  <img class="qquiz-personality-image" src="" />
	</div>
	<div class="qquiz-personality-halfblock">
	  <div class="qquiz-personality-desc"></div>
	</div>
      </div>
      <div class="qquiz-personality-social">
	<h4><?=v("quiz_share_result")?></h4>
	<div class="qquiz-personality-fbshare qquiz-personality-share" onclick="castFacebookShareDialog();">
	  <img src="<?=get_template_directory_uri()?>/images/social/facebook64.png" class="" alt="Share on Facebook"> 
	  <span>Facebook</span>
	</div>
	<div class="qquiz-personality-twshare qquiz-personality-share" onclick="castTwitterShareDialog();">
	  <img src="<?=get_template_directory_uri()?>/images/social/twitter64.png" class="" alt="Share on Twitter"> 
	  <span>Twitter</span>
	</div>	
      </div>
    </div>
  </div>
</div>
