<?php 
	// Default meta box for personalities definitions
	global $wpdb;
	$questions = array();
	$answers = array();

	$columncount = 5;

	if (!empty($_GET['post'])) {
		$q = '
			SELECT * from '.$wpdb->get_blog_prefix().'qquiz_post_quest
			WHERE qquiz_post_id = '.$_GET['post'].'
		;';

		$result = $wpdb->get_results($q);
		
		foreach ( $result as $pers ) {
			array_push($questions, $pers);
		}

		$q = '
			SELECT * from '.$wpdb->get_blog_prefix().'qquiz_quest_answer
			WHERE qquiz_post_id = '.$_GET['post'].'
		;';

		$result = $wpdb->get_results($q);
		
		foreach ( $result as $pers ) {
			array_push($answers, $pers);
		}
	} 
?>

	<script> 
		// Filled on update, blank on create
        // Load questions and answers
		var questions = [
			<?php
				for ($i = 0; $i < count($questions); $i++) {
					echo ($i == 0 ? "" : ",") . 
						'{"index":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($questions[$i]->qquiz_quest_index)).
						 '","desc":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($questions[$i]->qquiz_quest_text)).
						 '","attach":"'.$questions[$i]->qquiz_quest_attach.
						 '","credname":"'.$questions[$i]->qquiz_quest_cred_name.
						 '","credlink":"'.$questions[$i]->qquiz_quest_cred_link.'"}';
				}				
			?>
		];
		
        var answers = [
			<?php
				for ($i = 0; $i < count($answers); $i++) {
					echo ($i == 0 ? "" : ",") . 
						'{"index":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($answers[$i]->qquiz_quest_index)).
						 '","desc":"'.str_replace(array("\r\n", "\n", "\r"), ' ', addslashes($answers[$i]->qquiz_answer_text)).
						 '","attach":"'.$answers[$i]->qquiz_answer_attach.
						 '","type":"'.$answers[$i]->qquiz_answer_type.
						 '","label":"'.$answers[$i]->qquiz_pers_label.
						 '","credname":"'.$answers[$i]->qquiz_answer_cred_name.
						 '","credlink":"'.$answers[$i]->qquiz_answer_cred_link.'"}';
				}				
			?>
		];
		
        
		// Once DOM is ready
		jQuery(function() {
			// HTML generator and event manager
			var generator = new QquizHTMLGenerator();
			var eventManager = new QquizEventManager();

			// For each personalities
			for (var i = 0; i < questions.length; i++) {
				jQuery('.questions-list-wrapper').append(
					generator.generateNewQuestion(questions[i])
				);
			}
            
			// For each personalities
			for (var i = 0; i < answers.length; i++) {
				jQuery('.questions-list-wrapper .question-wrapper').eq(answers[i].index-1)
                    .find('.answers-list')
                    .append(
					   generator.generateNewAnswer(undefined, answers[i])
				    );
                
                // Replace button
                button = jQuery('.questions-list-wrapper .question-wrapper').eq(answers[i].index-1)
                    .find('.answers-list .qquiz-btn-addanswer');
                
                button.parent().append(button);
                    
			}            

			// Bind attachment click
			eventManager.bindAllQAThumbnailClicks();
		});
	</script>

	<div>
		<div class="questions-list-wrapper">
			<!-- Template goes here -->

		</div>
	
		<footer>
			<button id="qquiz-btn-addquest" class="button">Add Question</button>
		</footer>
	</div>	
<?php ?>
